# classify.py
# main file for running the clustering stuff

####################################
import shapely.geometry
import fiona
from sklearn.cluster import KMeans
import pandas as pd
from fiona import collection
from collections import OrderedDict

#####################################

# no functions, sorry


with fiona.open('./forest/TRI_Cell.shp', 'r') as source:
    # store info about the file for later use
    source_driver = source.driver
    source_crs = source.crs
    source_schema = source.schema

    tree = []

    for f in source:
        # loop through polygons and get the relevant information we went

        # skip invalid geometries
        if len(f['geometry']['coordinates'][0]) < 3:
            continue

        # get tree type
        ecoclass = f['properties']['ECOCLASS']

        # get centroid coordinates
        pol = shapely.geometry.Polygon(f['geometry']['coordinates'][0])
        xcoord = list(pol.centroid.coords)[0][0]
        ycoord = list(pol.centroid.coords)[0][1]

        # write the data
        tree.append((xcoord, ycoord, ecoclass))
    
    # make a dataframe with the relevant data
    cent = pd.DataFrame(tree, columns = ['xcoord', 'ycoord', 'ecoclass'])
    
    # do the kmeans clustering
    km = KMeans(n_clusters = 7, n_init = 10)
    km.fit(cent[['xcoord', 'ycoord']])
    y_pred = km.predict(cent[['xcoord', 'ycoord']])

    # add the y_pred to the centroid dataframe
    cent['clusterID'] = y_pred

    # now output a new shapefile with the clustered data
    # first define the schema we will use for the metadata
    new_schema = {'geometry': 'Point',
                'properties': OrderedDict([('ECOCLASS', 'str:5'), ('clusterID', 'int:5')])}

    # now write the shapefile
    with fiona.open(
            './clustered_forest/clustered_forest.shp',
            'w',
            driver = source_driver,
            crs = source_crs,
            schema = new_schema) as dest:
        for index, row in cent.iterrows():
            point = shapely.geometry.Point(row['xcoord'], row['ycoord'])
            dest.write({
                'geometry': shapely.geometry.mapping(point),
                'properties': {'ECOCLASS': '{}'.format(row['ecoclass']),
                                'clusterID': '{}'.format(row['clusterID'])}
                })

    def plots():
        # function for doing the plots
        import matplotlib.pyplot as plt
        #%matplotlib inline
        cent.plot.scatter(x = 'xcoord', y = 'ycoord', c = 'red')
        plt.scatter(cent['xcoord'], cent['ycoord'], c = y_pred)

    #plots()

