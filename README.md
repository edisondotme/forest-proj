# Forest tree correlation project

This will serve as the repository for any documents, code, or any otherfiles needed for this project.

![uncluster](polymap.png "uncluster")

![cluster](sklearn_cluster.png "cluster")


## Notes

National forest in washington - olympic national forest
Tree class, year of origin, major species, class, and size
- analyse distribution around the state

Data from national forest service
http://www.fs.fed.us/r6/data-library/gis/olympic/
Atlas of US Trees
http://esp.cr.usgs.gov/data/little/

shapefile to csv points
I'll gave to get centroids for our polygons
http://gis.stackexchange.com/questions/8844/get-list-of-coordinates-for-points-in-a-layer

Our project is a multi-faceted exercise in GIS, data science, and analysis. We wish to analyze clustering of trees in Olympic National Forest in Washington state. The outline for our project is as follows:
* Feed in tree location data into a python algorithm that will attempt to find clusters of trees and make a guess as to whether or not a certain cluster is a different species from neighboring clusters.
* We will then compare this output to the tree location + species data to assess whether or not our model is good at determining the clustering of tree species.
* Then we'll make a process for determining whether or not each tree species is actually clustered using spatial measures of clustering like spatial autocorrelation. We'll use python and perhaps ESRI geospatial modules. But also maybe not.
