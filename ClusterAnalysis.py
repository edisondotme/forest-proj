import arcpy, math, scipy
from arcpy import env
from scipy import stats
envir = "C:/Users/mansari2/Downloads/TRI_CELL" #arcpy.SetParameterAsText(0)
env.workspace = envir

fc = "TRI_CELL.shp" #arcpy.SetParameterAsText(1)
arcpy.CreateFeatureLayer_management(fc, sites)

speciesField = "CD" #arcpy.SetParameterAsText(2)

#Validate speciesField
doesntExist = True
#Create field
if (doesntExist):
	arcpy.AddField_management(sites, speciesField, "SHORT")
	arcpy.CalculateField_management(sites, speciesField, 0)
	arcpy.CalculateField_management(sites, speciesField, 1,'"ECOCLASS" = ' speciesField)
	print "Created field named " + speciesField

result = arcpy.GetCount_management(sites)
N = int(result.getOutput(0))

summed_total = 0
with arcpy.da.SearchCursor(fc, [speciesField]) as cursor:
	for row in cursor:
		summed_total = summed_total + row[0]
B = summed_total
del row, cursor

W = N - B

arcpy.CreateFeatureLayer_management(sites, notSpecies, speciesField = 0)
arcpy.SelectByLocation_analysis(sites, "BOUNDARY_TOUCHES", notSpecies)

result = arcpy.GetCount_management(sites)
BWCount = int(result.getOutput(0))

JoinSum = 0
J = 0
arcpy.CreateFeatureLayer_management(fc, sites2)
with arcpy.da.SearchCursor(fc, [speciesField]) as cursor:
	for row in cursor:
		arcpy.SelectByLocation_analysis(sites, "BOUNDARY_TOUCHES", sites2)
		Li = int(arcpy.GetCount_management(sites).getOutput(0))
		JoinSum += Li*(Li-1)
		J = J + Li/2
del row, cursor

#Calculate statistical parameters, mean and variance for z-test
BWExpected = ((2.0*J*B*W)/(N*(N-1)))
Variance = BWExpected - BWExpected^2 + (B * W * (JoinSum/(N*(N-1)))) + (4 * (J*(J-1) - JoinSum) * (B*(B - 1)* W * (W-1))/(N*(N-1)*(N-2)*(N-3)))

Zstat = (BWCount - BWExpected)/math.sqrt(Variance)
pval = stats.norm.cdf(math.abs(Zstat), 0, 1)

