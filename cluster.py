"""
Run clustering analysis on a shapefile

Pipeline: 
    Read in shapefile
    use shapely to get centroids of all polygons
    run clustering algorithms on the points

INPUT: Shapefile with polygons
OUTPUT: clustered geoJSON with labels. Also maybe some plots
"""
from shapely.geometry import shape
import fiona

def convertData():
    # convert the data
   c = fiona.open(r'~/media/Documents/UIUG/2016/GEOG 380/forest-proj/TRI_Cell/TRI_Cell.shp') 
   pol = c.next()
   geom = shape(pol['geometry'])

   multi = shapely.geometry.MultiPolygon([geom for pol in c])
   
   return(multi)




